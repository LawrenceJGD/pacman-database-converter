#!/usr/bin/python

"""Extractor de datos de la base de datos de Pacman."""

import os


def deps_dict(text_list, separator=None):
    """Procesa provides, depends y optdepends y devuelve sus datos.

    Args:
        text_list: lista de strings con los nombres de las dependencias.
        separator: caracteres para separar los strings

    Returns:
        Diccionario con el nombre del paquete como clave y su
        descripción como valor o None si no tiene.
    """
    if text_list is None:
        return None
    if separator is None:
        return text_list

    provides = {}
    for text in text_list:
        splitted_text = text.split(separator, maxsplit=1)
        if len(splitted_text) > 1:
            provides[splitted_text[0]] = splitted_text[1]
        else:
            provides[text] = None
    return provides


def deps_list(text_list):
    depends = []
    for text in text_list:
        splitted_text = text.split(': ', maxsplit=1)
        if len(splitted_text) == 2:
            depends.append(splitted_text)
        else:
            depends.append([text, None])
    return depends


def solo_list(text_list):
    """Devuelve el primer elemento de una lista o None si está vacía."""
    return text_list[0] if text_list else None


def solo_int_list(text_list):
    """Devuelve el primer str de una lista como int o None."""
    return int(text_list[0]) if text_list else None


def list_or_none(text_list):
    """Devuelve text_list si no es None sino None."""
    return None if text_list is None else text_list


def none_text(text_list, text):
    """Devuelve None si el primer elemento de text_list es igual a text."""
    return None if text_list[0] == text else text_list[0]


def get_desc(descfile):
    """Procesa el archivo desc y devuelve sus datos.

    Args:
        descfile: ruta del archivo desc.

    Returns:
        Diccionario con los datos extraidos del archivo desc.
    """

    # Extrae toda la información del archivo desc.
    with open(descfile) as desc:
        text_data = desc.read()

    # Divide el archivo en pedazos que contienen los nombres de los
    # campos y sus datos
    text_data = text_data.split('\n\n')
    raw_data = {}

    # Pasa cada campo a data usando como clave el nombre de cada campo.
    # y en el valor los valores de este.
    for field in text_data:
        field = field.splitlines()
        if field:
            raw_data[field[0]] = field[1:]

    return raw_data


def get_packages_list():
    """Devuelve la lista de los directorios de paquetes."""
    packages = []
    with os.scandir('/var/lib/pacman/local') as pacman:
        for entry in pacman:
            if entry.is_dir():
                packages.append(entry.path)
    return packages
