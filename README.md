# Convertidor de base de datos de Pacman

Este es un software escrito en Python desarrollado para convertir la base de
datos del gestor de paquetes Pacman a otros formatos de base de datos como DBM
(utilizando el módulo Shelve) y SQLite.

Este script solamente lo he escrito como curiosidad para aprender el
funcionamiento de SQLite y DBM y también un poco el funcionamiento interno de
Pacman.

# Scripts en el repositorio:

* [sqlite_database](./sqlite_database.py): convertidor de la base de datos a
  SQLite 3.

  Actualmente es necesario usar:
  `python -m pacman_database_converter.sqlite_database`

* [shelve_database](./shelve_database.py): convertidor de la base de datos a DBM
  (usando el módulo shelve).

  Actualmente es necesario usar:
  `python -m pacman_database_converter.sqlite_database`

* [pacman_metadata_fields](./pacman_metadata_fields.py): analiza la base de
  datos de Pacman y muestra todos los campos usados.

* [mtree_parser](./mtree_parser.py): interpreta los archivos mtree que contienen
  metadatos sobre los archivos que pertenecen al paquete.

* [field_search.awk](./field_search.awk): permite buscar en la base de datos de
  Pacman para ver el funcionamiento de cada uno de los campos. En los
  comentarios del script se especifica como se utiliza.

## Cosas por hacer:

* Agregar soporte para procesamiento multihilo para poder sobrepasar el cuello
  de botella causado por I/O.

* Crear un mejor método para poder ejecutar cada los dos convertidores de la
  base de datos.

* Agregar soporte para decodificar los archivos mtree y poder agregar sus datos
  a la base de datos.
