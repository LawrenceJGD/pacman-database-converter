#!/usr/bin/python

"""Conversor de la base de datos de pacman a GDBM O NDBM."""

import os.path
import pathlib
import shelve

from pacman_database_converter import extract
from pacman_database_converter import mtree


def decode_desc(descfile):
    """Procesa el archivo desc y devuelve sus datos.

    Args:
        descfile: ruta del archivo desc.

    Returns:
        Diccionario con los datos extraidos del archivo desc.
    """

    raw_data = extract.get_desc(descfile)

    # Establece la como se convertirá la información que se devolverá.
    # 0: nombre de la clave del diccionario de data.
    # 1: nombre del campo en raw_data.
    # 2: función que convertirá el dato o una tupla con el primer valor
    #    siendo la función y el segundo un argumento para esta.
    structure = (
        ('name', '%NAME%', extract.solo_list),
        ('version', '%VERSION%', extract.solo_list),
        ('base', '%BASE%', extract.solo_list),
        ('desc', '%DESC%', extract.solo_list,),
        ('url', '%URL%', extract.solo_list),
        ('arch', '%ARCH%', extract.solo_list),
        ('builddate', '%BUILDDATE%', extract.solo_int_list),
        ('installdate', '%INSTALLDATE%', extract.solo_int_list),
        ('packager', '%PACKAGER%', (extract.none_text, 'Unknown Packager')),
        ('size', '%SIZE%', extract.solo_list),
        ('license', '%LICENSE%', extract.solo_list),
        ('provides', '%DEPENDS%', (extract.deps_dict, '=')),
        ('depends', '%DEPENDS%', (extract.deps_dict, ': ')),
        ('optdepends', '%OPTDEPENDS%', (extract.deps_dict, ': ')),
        ('reason', '%REASON%', bool),
        ('groups', '%GROUPS%', extract.list_or_none),
        ('conflicts', '%CONFLICTS%', extract.list_or_none),
        ('validation', '%VALIDATION%', (extract.none_text, 'none')),
    )

    data = {}
    # Itera sobre structure convirtiendo los datos según cada
    for key, field_name, converter in structure:
        if isinstance(converter, tuple):
            value = converter[0](raw_data.get(field_name), converter[1])
        else:
            value = converter(raw_data.get(field_name))
        if value is not None:
            data[key] = value

    return data


def decode_files(descfile):
    """Extrae la lista de backup del paquete en package_dir.

    Args:
        descfile: ruta del archivo desc.

    Returns:
        Diccionario con los datos extraidos del archivo files.
    """

    raw_data = extract.get_desc(descfile)
    if '%BACKUP%' not in raw_data:
        return None

    data = []
    for text in raw_data['%BACKUP%']:
        text = text.rsplit('\t', 1)
        data.append((
            os.path.normcase(os.path.normpath(text[0])),
            None if text[1] == '(null)' else bytes.fromhex(text[1])
        ))

    return data


def get_metadata(package_dir):
    """Extrae los metadatos del paquete en package_dir y les da formato.

    Args:
        package_dir: ruta del paquete en la base de datos de pacman.

    Returns:
        Diccionario con todos los metadatos del paquete.
    """

    package_dir = pathlib.PurePath(package_dir)
    metadata = {}

    # Extrae la información del archivo desc que siempre está presente.
    metadata['desc'] = decode_desc(package_dir / 'desc')

    backup_data = decode_files(package_dir / 'files')
    if backup_data:
        metadata['backup'] = backup_data

    mtree_data = mtree.read_mtree_gzip(
        package_dir / 'mtree',
        normalize=True,
        exclude=('.BUILDINFO', '.PKGINFO', '.INSTALL')
    )
    if mtree_data:
        metadata['files'] = mtree_data

    # Extracción de cada archivo.
    for filename in ('install', 'changelog'):
        try:
            file = open(package_dir / filename)
        except FileNotFoundError:
            continue
        else:
            with file:
                metadata[filename] = file.read()

    return metadata


def main():
    """Función principal."""

    with shelve.open('pacman_db.db') as database:
        for package in extract.get_packages_list():
            database[os.path.basename(package)] = get_metadata(package)
            #database.sync()


# estructura = {
#     'hyperrogue-11.2d-1': {
#         'desc': {
#             'name': 'hyperrogue',
#             'version': '11.2d-1',
#             'base': 'hyperrogue',
#             'desc': ('You are a lone outsider in a strange, non-Euclidean '
#                      'hyperbolic world'),
#             'url': 'http://www.roguetemple.com/z/hyper.php',
#             'arch': 'x86_64',
#             'builddate': 1571161022,
#             'installdate': 1571181777,
#             'packager': 'yo',
#             'size': 10432512,
#             'license': ['GPL', 'custom'],
#             'validation': [],
#             'depends': [
#                 'sdl_gfx', 'sdl_ttf', 'sdl_mixer', 'mesa', 'ttf-dejavu'
#             ],
#             'optdepends': {'hyperrogue-music': 'Hyperrogue music'}, #
#             # Campos opcionales:
#             # 'provides': None,
#             # 'reason': False, # Significa si fue instalado
#             #                  # explicitamente o no, 1 (True) es que es
#             #                  # una dependecia, 0 (False) no lo es.
#             # 'conflicts': ['hyperrogue-git'],
#             # 'groups': []
#         },
#         'files': [
#             {'gid': 0, 'mode': 493, 'path': 'usr', 'time': 1571161022.0,
#              'type': 'dir', 'uid': 0},
#             {'gid': 0, 'mode': 493, 'path': 'usr/bin', 'time': 1571161022.0,
#              'type': 'dir', 'uid': 0},
#             {'gid': 0, 'md5': '7eae1f0bdb12cb0970ec5db1f8871d42',
#              'mode': 493, 'path': 'usr/bin/hyperrogue',
#              'sha256': '79a71e90e8dd485ad4b9d9519eb97c07576c60328d0cd319761833934177caef',
#              'size': 8545096, 'time': 1571161022.0, 'type': 'file', 'uid': 0}
#         ],
#         # 'backup': [{'path': 'etc/config', 'hash': '03423ab3cf'}]
#         # 'install': 'Bash Script',
#     }
# }


if __name__ == '__main__':
    main()
