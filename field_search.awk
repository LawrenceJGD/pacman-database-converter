#!/usr/bin/gawk -f
BEGIN {
    FS="\n"
    RS="\n\n"
}
{
# Hay tres variables que declarar para que funcione:
# field: Nombre del campo.
# text: texto que se comparará. 
# arg: comparación que se hará.
#     gt: la cuenta de campos es mayor que text.
#     ge: la cuenta de campos mayor o igual que text.
#     eq: el campo es igual a text.
#     ne: el campo no es igual que text.
#     re: el campo coincide con la expresión regular.
#     nr: el campo no coincide con la expresión regular.
#
# Ej: field_search.awk field=%DEPENDS% arg=gt text=3 /var/lib/pacman/local/glib2-2.64.3-2/desc

# Comprueba que las variables estén declaradas.
if (field == "") {
    print "ERROR: field no está declarado" > "/dev/stderr"
    exit 1
} else if (text == "") {
    print "ERROR: text no está declarado" > "/dev/stderr"
    exit 1
} else if (arg == "") {
    print "ERROR: arg no está declarado" > "/dev/stderr"
    exit 1
} else if (arg !~ /^(gt|ge|eq|ne|re|nr)$/) {
    print "ERROR: arg tiene un argumento inválido" > "/dev/stderr"
    exit 1
}

printed = 0
record_name = $1

if (record_name == field) {
    for (i = 1; i <= NF; i++) {
        # Hay que recordar que el número en NF también incluye a la cabecera.
        if \
                ((arg == "gt" && NF > text) || (arg == "ge" && NF >= text) \
                || (arg == "lt" && NF < text) || (arg == "le" && NF <= text) \
                || (arg == "eq" && $i == text) \
                || (arg == "ne" && $i != "" && $i != text) \
                || (arg == "re" && $i ~ text) || (arg == "nr" && $i !~ text)) {
            if ($i == field) {
                continue
            }
            if (printed == 0) {
                printf("%s:\n", FILENAME)
                printed = 1
            }
            print $i
        }
    }
}
}
