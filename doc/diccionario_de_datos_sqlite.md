# Diccionarios de Datos

## package

Contiene información general de cada uno de los paquetes instalados.

* package_id (INTEGER, PK, NN): ID del paquete, se genera automáticamente por la
  base de datos.

* fullname (TEXT, NN, U): nombre del paquete, solo letras minúsculas, números,
  y los caracteres "@.\_+-", no está permitido que los nombres empiecen con
  puntos o guiones. Regex: `^(?![.-])[a-z@._+-]$`

* composed_name (TEXT, NN, U): nombre del paquete más la versión, se compone así
  `<package_name>-<version>` (sin los "<" y ">").

* pkg_description (TEXT, NN): descripción del paquete.

* reason (BOOLEAN (INTEGER), NN): motivo de su instalación: TRUE (1) si es una
  dependencia, FALSE (0) si fue instalado explícitamente.

* version (TEXT, NN): versión del paquete.

* base (TEXT): nombre del paquete base.

* url (TEXT, NN): enlace hacia el sitio web oficial del paquete.

* arch (TEXT, NN): arquitectura para la que fue construido.

* builddate (TIMESTAMP (INT), NN): tiempo de UNIX en el que se compiló el
  paquete.

* installdate (TIMESTAMP (INT), NN): tiempo de UNIX en el que se instaló el
  paquete.

* packager (TEXT): nombre del empaquetador que creo el paquete.

* size: (INTEGER): tamaño que ocupa el paquete.

* validation (TEXT): método usado para validar el paquete.

* install (TEXT): script en Bash con funciones que deben ser ejecutadas
  antes/después es instalar/actualizar/desinstalar el paquete.

* changelog (TEXT): changelog del paquete.

## dependency

Contiene las dependencias de cada uno de los paquetes instalados.

* package_id (INTEGER, PK, FK (package_id en package), NN): ID del paquete

* dependency_name (TEXT, PK, NN): nombre de la dependencia.

## opt_dependency

Contiene las dependencias opcionales de cada uno de los paquetes instalados.

* package_id (INTEGER, FK (package_id en package), NN): ID del paquete

* optdep_name (TEXT, NN): nombre de la dependencia opcional.

* optdep_description (TEXT): descripción de la dependencia opcional.

## provides

Contiene las características proveídas por los paquetes instalados.

* package_id (INTEGER, PK, FK (package_id en package), NN): ID del paquete

* provided_name (TEXT, PK, NN): nombre de la característica proveída.

## license

Contiene las licencias de los paquetes instalados.

* package_id (INTEGER, PK, FK (package_id en package), NN): ID del paquete

* license_name (TEXT, PK, NN): nombre de la licencia.

## group

Contiene los grupos a los que pertenecen los paquetes.

* package_id (INTEGER, PK, FK (package_id en package), NN): ID del paquete

* group_name (TEXT, PK, NN): nombre del grupo.

## file

Es una lista de archivos que pertenecen a los paquetes.

* package_id (INTEGER, PK, FK (package_id en package), NN): ID del paquete

* path (TEXT, PK, NN): ruta del archivo.

* type (INTEGER, NN): tipo de archivo, limitado a un rango desde el 0 hasta el
  6, estos significan:

  0. Block special device
  1. Character special device
  2. Directory
  3. FIFO.
  4. Regular file
  5. Symbolic link
  6. Socket.

* mode (INTEGER, NN): número octal que representa los permisos del archivo.

* size (INTEGER, NN): tamaño en bytes del archivo.

* time (FLOAT, NN): marca de tiempo de la última vez que fue modificado el
  archivo.

* md5_digest (BLOB): digest MD5 del archivo (si es de tipo archivo (4)).

* sha256_digest (BLOB): digest SHA-256 del archivo (si es de tipo archivo (4)).

* uid (INTEGER, NN): ID del usuario dueño del archivo.

* gid (INTEGER, NN): ID del grupo dueño del archivo.

* link (TEXT): Ruta objetivo del archivo si type es un enlace simbólico (5).

## backup

Es una lista de archivos que deben ser respaldados cuando se modifica el
paquete.

* package_id (INTEGER, FK (package_id en package), NN): ID del paquete

* backup_path (TEXT, NN): ruta del archivo a ser respaldado.

* checksum (BLOB, NN): hash MD5 del archivo original para comprobar si ha sido
  modificado o no.
