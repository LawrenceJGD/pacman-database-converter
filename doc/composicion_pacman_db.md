# Composición de la base de datos de Pacman

* Es una base de datos No-SQL.

* Se ubica en `/var/lib/pacman/`

* Hay dos directorios que tiene dos tipos de base de datos, la base de datos
  local (`/var/lib/pacman/local`) donde está la información de los paquetes
  instalados localmente, y luego está sync (`/var/lib/pacman/sync`) donde está
  la información de los paquetes en el repositorio.

* En sync hay varios archivo con extensión `.db` pero en realidad son archivo
  `.tar.gz` con la información de los paquetes de cada uno de los repositorios.

* En local hay un montón de directorios, cada uno es para cada paquete y sus
  nombres están compuestos de la siguiente manera:
  `<nombre_del_paquete>-<version>` (sin los símbolo de mayor y menor qué).

* En cada directorio pueden haber hasta 5 archivos distintos:

  * `desc` (obligatorio): Es un archivo de texto, este contiene datos sobre
    el paquete, campo se divide utilizando cabeceras, estas comienzan y
    terminan con % y están en mayúsculas, y los datos se insertan por líneas,
    cuando terminan dejan una línea vacía y luego la siguiente cabecera:

    * `%NAME%`: Nombre del paquete.
    * `%VERSION%`: Versión del paquete.
    * `%SIZE%` (No obligatorio): Tamaño del paquete.
    * `%REPLACE%` (No obligatorio): A que paquete reemplaza.
    * `%DEPENDS%` (No obligatorio): Dependencias.
    * `%OPTDEPENDS%` (No obligatorio): Dependencias opcionales.
    * `%PROVIDES%` (No obligatorio): Lo que provee.
    * `%REASON%` (No obligatorio): La razón por la que se instaló.
    * `%PACKAGER%`: El nombre y correo del empaquetador.
    * `%DESC%`: Descripción del paquete.
    * `%CONFLICTS%` (No obligatorio): Paquetes con los que tiene conflictos.
    * `%BUILDDATE%`: Fecha de la compilación.
    * `%INSTALLDATE%`: Fecha de instalación.
    * `%ARCH%`: Arquitectura para la que se compiló.
    * `%GROUPS%` (No obligatorio): Grupos a los que pertenece.
    * `%LICENSE%` (No obligatorio): Licencias del paquete.
    * `%URL%`: Enlace a la página oficial.
    * `%VALIDATION%`: Métodos de validación del paquete.

  Ejemplo:

      %NAME%
      openxcom-extendend

      %VERSION%
      6.5.1-1

      %DEPENDS%
      sdl_mixer
      sdl_gfx
      sdl_image
      sdl
      yaml-cpp
      libgl
      hicolor-icon-theme
      gcc-libs

  * `files` (obligatorio): Es una lista de los archivos que incluye el paquete
    y también dice que archivos deben ser respaldados por si el usuario los
    haber modificado. Funciona de manera similar con los campos y cabeceras de
    desc:

    * `%FILES%`: Archivos que contiene el paquete.
    * `%BACKUP%` (no obligatorio): Archivos que deben ser respaldados, cada
      dato tiene el siguiente formato:
      `<ruta del archivo>\t<md5sum del archivo>`

  * `mtree` (obligatorio): Un archivo de texto comprimido con gzip que contiene
    metadatos sobre todos los archivos que pertenecen al paquete, estos son:

    * Ruta del archivo.
    * Tipo de archivo: block, char, dir, fifo, file, link y socket.
    * UID.
    * GID.
    * Permisos en forma de número octal.
    * Marca de tiempo de la última vez que fue modificado.
    * Tamaño del archivo en bytes.
    * Digest MD5 del archivo.
    * Digest SHA-256 del archivo.
    * Objetivo al que apunta en caso de que sea un enlace.

  * `changelog` (no obligatoro): El changelog del paquete.

  * `install` (no obligatorio): Script en Bash que contiene funciones que se
    ejecutan antes/después de actualizar/instalar/desinstalar el paquete.
