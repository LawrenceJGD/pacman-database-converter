#!/usr/bin/python3

"""Conversor de la base de datos de pacman a SQLite."""

import itertools
import pathlib
import sqlite3

from pacman_database_converter import extract
from pacman_database_converter import mtree


def decode_desc(descfile, package_id):
    """Procesa el archivo desc y devuelve sus datos.

    Args:
        descfile: ruta del archivo desc.
        package_id: ID del paquete para SQL.

    Returns:
        Diccionario con los datos extraidos del archivo desc.
    """

    raw_data = extract.get_desc(descfile)

    # Establece la como se convertirá la información que se devolverá.
    # 0: nombre del campo en raw_data.
    # 1: función que convertirá el dato o una tupla con el primer valor
    #    siendo la función y el segundo un argumento para esta.
    package_structure = (
        ('%NAME%', extract.solo_list),
        ('%DESC%', extract.solo_list),
        ('%REASON%', bool),
        ('%VERSION%', extract.solo_list),
        ('%BASE%', extract.solo_list),
        ('%URL%', extract.solo_list),
        ('%ARCH%', extract.solo_list),
        ('%BUILDDATE%', extract.solo_int_list),
        ('%INSTALLDATE%', extract.solo_int_list),
        ('%PACKAGER%', (extract.none_text, 'Unknown Packager')),
        ('%SIZE%', extract.solo_int_list),
        ('%VALIDATION%', (extract.none_text, 'none'))
    )

    # Establece el resto de tablas a ser convertidas
    # 0: nombre del campo en raw_data.
    # 1: nombre de la tabla a la que corresponden los datos.
    tables_structure = (
        ('%PROVIDES%', 'provides'),
        ('%DEPENDS%', 'dependency'),
        ('%OPTDEPENDS%', 'opt_dependency'),
        ('%PROVIDES%', 'provides'),
        ('%LICENSE%', 'license'),
        ('%GROUPS%', 'group')
    )

    data = {'package': [package_id]}

    # Convierte y agrega los datos para la tabla de package.
    for field_name, converter in package_structure:
        if isinstance(converter, tuple):
            value = converter[0](raw_data.get(field_name), *converter[1:])
        else:
            value = converter(raw_data.get(field_name))
        data['package'].append(value)

    # Convierte el resto de tablas.
    for field, table in tables_structure:
        if field in raw_data:
            data[table] = [[package_id, i] for i in raw_data[field]]

    # Como OPTDEPENDS tiene la descripción junto al nombre hay que
    # procesarlo aparte.
    if '%OPTDEPENDS%' in raw_data:
        data['opt_dependency'] = [
            [package_id] + i for i in extract.deps_list(
                raw_data['%OPTDEPENDS%']
            )
        ]

    return data


def decode_files(descfile, package_id):
    """Procesa el archivo files y devuelve sus datos.

    Args:
        descfile: ruta del archivo files.
        package_id: ID del paquete para SQL.

    Returns:
        Diccionario con los datos extraidos del archivo files.
    """

    raw_data = extract.get_desc(descfile)
    if '%BACKUP%' not in raw_data:
        return None
    data = []
    for text in raw_data['%BACKUP%']:
        text = text.rsplit('\t', 1)
        text[1] = None if text[1] == '(null)' else bytes.fromhex(text[1])
        data.append([package_id] + text)

    return data


def get_metadata(package_dir, count):
    """Extrae los metadatos del paquete en package dir y les da formato.

    Args:
        package_dir: ruta del paquete en la base de datos de pacman.
        count: un número que se usará para asignar el ID del paquete.

    Returns:
        Diccionario con todos los metadatos del paquete.
    """

    package_dir = pathlib.PurePath(package_dir)
    metadata = decode_desc(package_dir / 'desc', count)
    if backup_data := decode_files(package_dir / 'files', count):
        metadata['backup'] = backup_data
        del backup_data

    mtree_data = mtree.read_mtree_gzip(
        package_dir / 'mtree',
        normalize=True,
        exclude=('.BUILDINFO', '.PKGINFO', '.INSTALL'),
        default={
            'id': count,
            'size': None,
            'md5': None,
            'sha256': None,
            'link': None
        }
    )

    metadata['file'] = mtree_data
    del mtree_data

    for filename in ('install', 'changelog'):
        try:
            file = open(package_dir / filename)
        except FileNotFoundError:
            metadata['package'].append(None)
        else:
            with file:
                metadata['package'].append(file.read())

    return metadata


def hash_to_bytes(hash_str, length):
    """Convierte un hash de número hexadecimales a bytes.

    Args:
        hash_str: string de números hexadecimales.
        length: tamaño que debe tener el resultado en bytes.

    Returns:
        El hash convertido en bytes.

    Raises:
        ValueError: si hash_str no es un número hexadecimal válido,
        cuando la cantidad de caracteres es impar, o el tamaño del
        resultado es distinta a length.
    """
    hash_bytes = bytes.fromhex(hash_str)
    if len(hash_bytes) == length:
        return hash_bytes
    raise ValueError('el tamaño de hash_str es distinto a length')


def main():
    """Función principal."""
    pacman_path = pathlib.Path(__file__).parent
    pacman_db = pacman_path.resolve() / 'pacman.sqlite3'

    try:
        database = sqlite3.connect(pacman_db.as_uri() + '?mode=rw', uri=True)

    except sqlite3.OperationalError:
        database = sqlite3.connect(pacman_db)
        with database, open(pacman_path / 'pacman_db.sql') as sql_script:
            database.cursor().executescript(sql_script.read())

    with database:
        database.cursor().execute('PRAGMA synchronous=OFF;').close()

    # Cambia las funciones de conversión de md5 y sha256 para que las convierta
    # en bytes y así guardar ambos hashes en sus columnas BLOB
    mtree.STRUCTURE['md5'] = lambda x: hash_to_bytes(x, 16)
    mtree.STRUCTURE['sha256'] = lambda x: hash_to_bytes(x, 32)

    for count, directory in enumerate(extract.get_packages_list(), 1):
        package_metadata = get_metadata(directory, count)

        with database:
            cursor = database.cursor()

            cursor.execute(
                """
                INSERT INTO "package"
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
                """,
                package_metadata.pop('package')
            )

            if 'file' in package_metadata:
                cursor.executemany(
                    """
                    INSERT INTO "file"
                    VALUES ( :id
                           , :path
                           , CASE :type
                                  WHEN 'block'  THEN 0
                                  WHEN 'char'   THEN 1
                                  WHEN 'dir'    THEN 2
                                  WHEN 'fifo'   THEN 3
                                  WHEN 'file'   THEN 4
                                  WHEN 'link'   THEN 5
                                  WHEN 'socket' THEN 6
                             END
                           , :mode
                           , :size
                           , :time
                           , :md5
                           , :sha256
                           , :uid
                           , :gid
                           , :link);
                    """,
                    package_metadata.pop('file')
                )

            for key, value in package_metadata.items():
                placeholder = ", ".join("?" * len(value[0]))
                cursor.executemany(
                    f'INSERT INTO "{key}" VALUES ({placeholder});',
                    value
                )
            cursor.close()

    with database:
        database.cursor().executescript(
            """
            PRAGMA synchronous=NORMAL;
            VACUUM;
            PRAGMA optimize;
            """
        ).close()

    database.close()


if __name__ == '__main__':
    main()
