#!/usr/bin/python

"""Buscador de campos de la base de datos de pacman

Revisa todos los archivos desc en /var/lib/pacman/local e imprime todos
los campos utilizados en todos los paquetes, es posible que existan más
campos pero que no estén siendo utilizados.
"""

import glob
import re

pattern = re.compile(r'^(?:%).+(?:%)$', re.MULTILINE)
fields = set()

for path in glob.iglob('/var/lib/pacman/local/*/desc'):
    with open(path) as file:
        fields |= set(pattern.findall(file.read()))

for i in fields:
    print(i)
