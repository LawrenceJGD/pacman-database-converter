"""Interprete del formato mtree

Las funciones read_mtree_file y read_mtree_stream devuelven una lista
de diccionarios que contienen la información del texto en formato mtree,
cada diccionrio está conformado con varias claves, todas excepto path
son opcionales por lo que pueden aparecer o no. Estos claves simbolizan
las palabras clave de mtree y estás son:

* path (str): string con la ruta del archivo.

* type (str): tipo de archivo, estos son: block, char, dir, fifo, file,
  link y socket.

* time (float): tiempo de la última vez en la que fue modificado.

* mode (int): número octal con los permisos del archivo.

* link (str): el objetivo del enlace simbólico si type es link.

* size (int): tamaño en bytes del archivo.

* nlink (int): número de enlaces duros que se espera que tenga el
  archivo.

* inode (int): el número del inodo.

* contents (str): ruta completa de un archivo que contiene el contenido
  de este archivo

* ignore (bool): ignora cualquier jerarquía de archivos por debajo de
  este archivo.

* nochange (bool): se asegura de que el archivo exista pero de lo
  contrario ignora todos los atributos.

* optional (bool): el arcivo es opcional. No se queja si no existe en la
  jerarquía de archivos.

* md5: digest MD5 del archivo.

* rmd160 (str): digest RIPEMD160 del archivo.

* sha1 (str): digest SHA-1 del archivo.

* sha256 (str): digest SHA-256 del archivo.

* sha384 (str): digest SHA-384 del archivo.

* sha512 (str): digest SHA-512 del archivo.

* device (list[str o int]): el número del dispositivo cuando type es
  block o char. Puede ser una lista de tres o cuatro elemente o un int.

* resdevice (list[str o int]): el número residente del dispositivo del
  archivo. Tiene el mismo formato que device.

* flags (list[str]): flags del sistema de archivos que tenga el archivo.

* gid (int): el grupo del archivo en forma numérica.

* gname (str): el grupo del archivo como un nombre simbólico.

* uid (int): el usuario dueño del archivo en forma numérica.

* uname (str): el usuario dueño del archivo como un nombre simbólico.

Ejemplo del resultado de la intepretación:

    [
        {
            'path': './usr/bin/python3',
            'type': 'link',
            'time': 1589739342.0,
            'mode': 511,
            'gname': 'root',
            'uname': 'root',
            'link': 'python3.8'
        },
        {
            'path': './usr/bin/python3.8',
            'type': 'file',
            'time': 1589739342.0,
            'mode': 493,
            'gname': 'root',
            'uname': 'root'
        }
    ]
"""

import codecs
import gzip
import os.path
import re


class MtreeDecoderError(ValueError):
    """Se levanta cuando sucede se detecta un error en el documento.

    Attributes:
        message: mensaje sin formatear que se mostrará.
        doc: ruta al documento o None. Si es None se omitirá el nombre
            del documento en el mensaje de la excepción.
        line: línea a partir de la cual sucede el error.
    """
    def __init__(self, message, doc, lineno):
        self.message = message
        self.doc = doc
        self.lineno = lineno
        if doc is None:
            formatted_message = f'{message} from line {lineno}'
        else:
            formatted_message = f'{message} in "{doc}" from line {lineno}'
        super().__init__(formatted_message)


def _check_type(filetype):
    """Revisa que el tipo de archivo sea correcto.

    Args:
        filetype: string del tipo de archivo.

    Returns:
        El mismo string que se entregó.

    Raises:
        ValueError: si filetype no es un tipo de archivo válido.
    """
    types = ('block', 'char', 'dir', 'fifo', 'file', 'link', 'socket')
    if filetype in types:
        return filetype
    raise ValueError('tipo de archivo incorrecto')


def _check_device(device):
    """Revisa que device sea correcto y lo convierte.

    Args:
        device: string del dispositivo.

    Returns:
        Lista de strings y/o int.

    Raises:
        ValueError: si device no tiene entre 3 o 4 campos o no es int.
    """
    try:
        return int(device)
    except ValueError:
        pass

    device = device.split(',')
    if len(device) not in range(3, 5):
        raise ValueError('device no tiene la cantidad apropiada de campos')

    for i, j in enumerate(device):
        try:
            device[i] = int(j)
        except ValueError:
            continue

    return device


def _decode_path(file):
    """Interpreta los caracteres escapados en las rutas de los archivos."""
    return codecs.escape_decode(file)[0].decode()
    # Usar esto si algún día codecs.unicode_escape_decode es retirado.
    #return (file.encode('latin-1')
    #            .decode('unicode_escape')
    #            .encode('latin-1')
    #            .decode('utf-8'))


#def _decode_path(file):
#    """Interpreta los caracteres escapados en las rutas de los archivos."""
#    def octal_to_bytes(match):
#        match = match.group(1)
#        return int(match, 8).to_bytes(length=1, byteorder='big')
#
#    escape_pattern = re.compile(rb'(?<!\\)\\([0-7]{3})')
#    file = file.encode('ascii')
#    return escape_pattern.sub(octal_to_bytes, file).decode()


def _get_alias(key):
    """Revisa si key es un alias para otra clave y la devuelve.

    Args:
        key: clave que se comprobará si es un alias.

    Returns:
        key o la palabra clave a la que referencia alias_key.
    """

    # Palabras clave de mtree que funcionan como alias para otras. La clave
    # se usará para la asignar valores mientras que el valor contiene los
    # diferentes alias que son sinónimos.
    aliases = {
        'md5': ('md5', 'md5digest'),
        'sha1': ('sha1', 'sha1digest'),
        'rmd160': ('rmd160', 'ripemd160digest', 'rmd160digest'),
        'sha256': ('sha256', 'sha256digest'),
        'sha384': ('sha384', 'sha384digest'),
        'sha512': ('sha512', 'sha512digest')
    }
    for alias, keywords in aliases.items():
        if key in keywords:
            return alias
    return key


def _parse_special_line(line, setted_keywords, default):
    """Interpreta las líneas /set y /unset

    Declara y borra valores por defecto para las palabras claves según
    el contenido de la línea

    Args:
        line: línea a analizar.
        setted_keywords: palabras claves que han sido declaradas con
            /set.

    Returns:
        Diccionario con nuevos valores declarados.
    """

    line = line.split()

    # /set
    if line.pop(0) == '/set':
        for option in [i.split('=', 1) for i in line]:

            # Comprueba las claves
            key = _get_alias(option[0])
            value = option[1] if len(option) == 2 else None

            if key not in VALID_KEYWORDS:
                raise ValueError(f'invalid keyword "{key}"')

            try:
                if value is None:
                    setted_keywords[key] = STRUCTURE[key]()
                else:
                    setted_keywords[key] = STRUCTURE[key](value)
            except ValueError:
                raise ValueError(f'invalid value for keyword "{key}"')

    # /unset
    else:
        del line[0]
        for key in line.split():
            key = _get_alias(key)

            if key not in VALID_KEYWORDS:
                raise ValueError(f'invalid keyword "{key}"')

            try:
                if default is not None and key in default:
                    setted_keywords[key] = default[key]
                else:
                    del setted_keywords[key]
            except KeyError:
                raise ValueError(
                    f'unsetted keyword "{key}" that was not previously setted'
                )

    return setted_keywords


def _parse_full_line(line, setted_keywords):
    """Intepreta una línea full.

    Args:
        Los mismos que _parse_special_line.

    Returns:
        Un diccionario que representa una entrada en el archivo mtree,
        más información en la descripción del módulo.
    """
    line = line.split()
    entry = setted_keywords.copy()
    entry['path'] = _decode_path(line.pop(0))

    for option in line:
        option = option.split('=', maxsplit=1)
        key = _get_alias(option[0])
        value = option[1] if len(option) == 2 else None

        if key not in VALID_KEYWORDS:
            raise ValueError(f'invalid keyword "{key}"')

        try:
            if value is None:
                entry[key] = STRUCTURE[key]()
            else:
                entry[key] = STRUCTURE[key](value)
        except ValueError:
            raise ValueError(f'invalid value for keyword "{key}"')

    return entry


def _parse_relative_line(line, setted_keywords, current_directory):
    """Interpreta líneas relativas.

    Args:
        Los mismos que _parse_special_line.

    Returns:
        Un diccionario que representa una entrada en el archivo mtree,
        más información en la descripción del módulo.
    """
    entry = _parse_full_line(line, setted_keywords)
    path = entry['path']

    if entry.get('type') == 'dir':
        current_directory = os.path.normpath(
            os.path.join(current_directory, path)
        )

    else:
        entry['path'] = os.path.normpath(os.path.join(current_directory, path))

    return entry, current_directory


def _mtree_reader(mtree, normalize, exclude, default):
    """Itera sobre los datos de mtree, los interpreta y los devuelve.

    Args:
        mtree: iterador que contiene datos en formato mtree. Cada
            iteración debe representar una línea de un archivo mtree. Si
            tiene el atributo name se utilizará como la ruta del archivo
            donde se ubica mtree para las excepciones.
        normalize: booleano para normalizar las rutas de los resultados
            con os.path.normcase y os.path.normpath.
        exclude: iterador con rutas que serán excluidas del diccionario
            de entradas.
        default: diccionario con los valores que tendrán por defecto las
            entradas del resultado en caso de que no tengan un valor.

    Returns:
        Lista de diccionarios que contienen cada entrada del archivo
        mtree. Para ver más sobre como está formado revise la
        documentacion del módulo en sí.

    Rases:
        MtreeDecoderError: cuando se detecta una falla en el formato de
            mtree.
    """

    def normalize_path(file_entry):
        """Normaliza las rutas de las entradas del archivo mtree."""
        if normalize:
            file_entry['path'] = os.path.normcase(
                os.path.normpath(file_entry['path'])
            )
        return file_entry

    if default is not None and not isinstance(default, dict):
        raise TypeError('default no es dict ni None')

    # Keywords que han sido configuradas con /set y /unset
    file = getattr(mtree, 'name', None)
    setted_keywords = {} if default is None else default
    current_directory = '.'
    previous_lines = None
    line_count = 0
    entries = []
    entry = None

    for line in mtree:
        line_count += 1
        # Líneas en blanco o comentarios.
        if _BLANK.match(line) or _COMMENT.match(line):
            continue

        if _BACKSLASHED_LINE.search(line):
            if previous_line is None:
                previous_line = line.rstrip('\\\n')
            else:
                previous_line += line.rstrip('\\\n')
            continue

        if previous_lines:
            line = previous_lines + line

        try:
            # Líneas con "/set" o "/unset".
            if _SPECIAL.match(line):
                setted_keywords = _parse_special_line(
                    line,
                    setted_keywords,
                    default
                )

            # Líneas con "..".
            elif _DOT_DOT.match(line):
                current_directory = os.path.normpath(
                    os.path.join(current_directory, '..')
                )

            # Líneas con la ruta entera.
            elif _FULL.match(line):
                entry = normalize_path(
                    _parse_full_line(line, setted_keywords)
                )

            # Líneas con una ruta relativa.
            elif _RELATIVE.match(line):
                entry, new_directory = _parse_relative_line(
                    line,
                    setted_keywords,
                    current_directory,
                )
                current_directory = os.path.normcase(
                    os.path.normpath(new_directory)
                )
                del new_directory

            # Líneas con formato erroneo
            else:
                print(line)
                raise MtreeDecoderError('invalid format', file, line_count)

        except ValueError as error:
            raise MtreeDecoderError(error.args[0], file, line_count)

        previous_line = None

        if entry is not None:
            if exclude is not None and entry['path'] not in exclude:
                entries.append(entry)
            entry = None

    return entries


def read_mtree_file(filepath, normalize=False, exclude=None, default=None):
    """Intepreta archivos mtree y devuelve sus datos.

    Args:
        filepath: string con la ruta hacia el archivo mtree.
        normalize: más información en _mtree_reader. False por defecto.
        exclude: más información en _mtree_reader. None por defecto.
        default: más información en _mtree_reader. None por defecto.

    Returns:
        Lista de diccionarios que contienen cada entrada del archivo
        mtree. Más información en la documentación el módulo en sí.

    Raises:
        MtreeDecoderError: cuando se detecta una falla en el documento
            en filepath.
        OSError: en caso de que haya un fallo al intentar abrir el
            archivo.
    """

    with open(filepath) as mtree_file:
        return _mtree_reader(mtree_file, normalize, exclude, default)


def read_mtree_gzip(filepath, normalize=False, exclude=None, default=None):
    """Intepreta archivos mtree comprimidos con gzip.

    Args:
        filepath: string con la ruta hacia el archivo mtree comprimido.
        normalize: más información en _mtree_reader. False por defecto.
        exclude: más información en _mtree_reader. None por defecto.
        default: más información en _mtree_reader. None por defecto.

    Returns:
        Lista de diccionarios que contienen cada entrada del archivo
        mtree. Más información en la documentación el módulo en sí.

    Raises:
        MtreeDecoderError: Cuando se detecta una falla en el documento
            en filepath.
        Cualquier error que pueda lanzar gzip.open.
    """

    with gzip.open(
            filepath,
            mode='rt',
            encoding='latin-1',
            errors='strict'
    ) as mtree_gzip:
        return _mtree_reader(mtree_gzip, normalize, exclude, default)


def read_mtree_string(
        mtree_string,
        normalize=False,
        exclude=None,
        default=None
    ):
    """Intepreta strings del formato mtree y devuelve sus datos.

    Args:
        mtree_string: string con el texto con formato mtree.
        normalize: más información en _mtree_reader. False por defecto.
        exclude: más información en _mtree_reader. None por defecto.
        default: más información en _mtree_reader. None por defecto.

    Returns:
        Lista de diccionarios que contienen cada entrada del archivo
        mtree. Más información en la documentación el módulo en sí.

    Raises:
        MtreeDecoderError: Cuando se detecta una falla de formato en
            mtree_string
    """

    return _mtree_reader(
        mtree_string.splitlines(),
        normalize,
        exclude,
        default
    )


# Convertidores para transformar los valores a sus tipos correspondientes.
# Puede ser modificada desde fuera del módulo para implementar nuevas formas de
# interpretar los datos o nuevas claves.
STRUCTURE = {
    'type': _check_type,
    'time': float,
    'size': int,
    'uid': int,
    'uname': str,
    'gid': int,
    'gname': str,
    'mode': lambda x: int(x, base=8),
    'link': _decode_path,
    'nlink': int,
    'ignore': lambda: True,
    'nochange': lambda: True,
    'optional': lambda: True,
    'inode': int,
    'contents': _decode_path,
    'flags': lambda x: None if x == 'none' else x.split(','),
    'device': _check_device,
    'resdevice': _check_device,
    'cksum': int,
    'md5': str,
    'rmd160': str,
    'sha1': str,
    'sha256': str,
    'sha384': str,
    'sha512': str,
}

# Patrones para identificar los tipos de líneas.
_BLANK = re.compile(r'^ *$')
_COMMENT = re.compile(r'^#')
_SPECIAL = re.compile(r'^/(un)?set(?= |$)')
_DOT_DOT = re.compile(r'^ *\.\.')
_RELATIVE = re.compile(r'^ *([^/ ]+)')
_FULL = re.compile(r'^ *.+/')
_BACKSLASHED_LINE = re.compile(r'(?<!\\)\\$', re.MULTILINE)

# Palabras clave de mtree válidas, ninguna debe repetirse.
VALID_KEYWORDS = (
    'cksum', 'device', 'contents', 'flags', 'gid', 'gname', 'ignore', 'inode',
    'link', 'md5', 'md5digest', 'mode', 'nlink', 'nochange', 'optional',
    'resdevice', 'ripemd160digest', 'rmd160', 'rmd160digest', 'sha1',
    'sha1digest', 'sha1digest', 'sha256', 'sha256digest', 'sha384',
    'sha384digest', 'sha512', 'sha512digest', 'size', 'time', 'type', 'uid',
    'uname'
)
