---------- Tablas Maestras ----------

CREATE TABLE "package" (
    "package_id"      INTEGER   NOT NULL PRIMARY KEY,
    "package_name"    TEXT      NOT NULL UNIQUE,
    "fullname"        TEXT      AS ("package_name" || '-' || "version") VIRTUAL,
    "pkg_description" TEXT      NOT NULL,
    "reason"          INTEGER   NOT NULL,
    "version"         TEXT      NOT NULL,
    "base"            TEXT,
    "url"             TEXT,
    "arch"            TEXT      NOT NULL,
    "builddate"       TIMESTAMP NOT NULL,
    "installdate"     TIMESTAMP NOT NULL DEFAULT (STRFTIME('%s', 'now')),
    "packager"        TEXT,
    "size"            INTEGER,
    "validation"      INTEGER,
    "install"         TEXT,
    "changelog"       TEXT,

    UNIQUE ("fullname")

    -- CHECK("package_name" REGEXP '(?a)\\A(?![-.]+)[a-z\\d@._+-]+\\Z')
);

/*CREATE TABLE "group_detail" (
    "group_id"   INTEGER NOT NULL PRIMARY KEY,
    "group_name" TEXT    NOT NULL UNIQUE
);*/

---------- Tablas de Transacciones ----------

CREATE TABLE "license" (
    "package_id"   INTEGER NOT NULL REFERENCES "package"("package_id"),
    "license_name" TEXT    NOT NULL,

    PRIMARY KEY ("package_id", "license_name")
) WITHOUT ROWID;

CREATE TABLE "group" (
    "package_id" INTEGER NOT NULL REFERENCES "package"("package_id"),
    "group_name" TEXT    NOT NULL,

    PRIMARY KEY ("package_id", "group_name")
) WITHOUT ROWID;

CREATE TABLE "dependency" (
    "package_id"      INTEGER NOT NULL REFERENCES "package"("package_id"),
    "dependency_name" TEXT    NOT NULL,

    PRIMARY KEY ("package_id", "dependency_name")
    --"optional"      BOOLEAN NOT NULL,
) WITHOUT ROWID;

CREATE TABLE "opt_dependency" (
    "package_id"         INTEGER NOT NULL REFERENCES "package"("package_id"),
    "optdep_name"        TEXT    NOT NULL,
    "optdep_description" TEXT
);

CREATE TABLE "file" (
    "package_id"    INTEGER NOT NULL REFERENCES "package"("package_id"),
    "path"          TEXT    NOT NULL,
    "type"          INTEGER NOT NULL,
    "mode"          INTEGER NOT NULL,
    "size"          INTEGER,
    "time"          REAL    NOT NULL,
    "md5_digest"    BLOB,
    "sha256_digest" BLOB,
    "uid"           INTEGER NOT NULL,
    "gid"           INTEGER NOT NULL,
    "link"          TEXT,

    CONSTRAINT "chk_path"   CHECK ("path" != ''),
    CONSTRAINT "chk_type"   CHECK ("type" BETWEEN 0 AND 6),
    CONSTRAINT "chk_mode"   CHECK ("mode" BETWEEN 0 AND 4095),
    CONSTRAINT "chk_size"   CHECK ("size" >= 0),
    CONSTRAINT "chk_md5"    CHECK (LENGTH("md5_digest")    = 16),
    CONSTRAINT "chk_sha256" CHECK (LENGTH("sha256_digest") = 32),
    CONSTRAINT "chk_uid"    CHECK ("uid" >= 0),
    CONSTRAINT "chk_gid"    CHECK ("gid" >= 0),
    CONSTRAINT "chk_link"   CHECK (CASE WHEN "link" IS NOT NULL
                                        THEN "type" = 5

                                        WHEN "type" = 5
                                        THEN "link" IS NOT NULL
                                   END),

    -- TODO: Chequear que md5_digest y sha256_digest no sean NULL cuando sean
    -- archivos normales.

    PRIMARY KEY ("package_id", "path")
);

CREATE TABLE "backup" (
    "package_id"  INTEGER NOT NULL REFERENCES "package"("package_id"),
    "backup_path" TEXT    NOT NULL,
    "checksum"    BLOB    CHECK (LENGTH("checksum") = 16)
);

CREATE TABLE "provides" (
    "package_id"       INTEGER NOT NULL REFERENCES "package"("package_id"),
    "provided_name"    TEXT    NOT NULL,

    PRIMARY KEY ("package_id", "provided_name")
) WITHOUT ROWID;

/*CREATE TABLE "required_version" (
    "package_id"    INTEGER NOT NULL REFERENCES "dependency"("package_id"),
    "dependency_id" INTEGER NOT NULL REFERENCES "dependency"("dependency_id"),
    "req_version"   TEXT    NOT NULL,
    "operator"      INTEGER NOT NULL CHECK("operator" BETWEEN 0 AND 4),

    PRIMARY KEY ("package_id", "dependency_id")
)*/

---------- Índices ----------

/*CREATE UNIQUE INDEX "IDX_package"
ON "package"("package_name", "pkg_description", "version");
